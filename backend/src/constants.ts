export const URI = 'mongodb://localhost:27017/taskmanager';
export const DATABASE = 'taskmanager';
export const TASKS_COLLECTION = 'tasks';