import { MongoClient, Db } from 'mongodb';
import { DATABASE, URI } from './constants';

export async function connectToMongoDB(): Promise<Db | void> {
    const client: MongoClient = new MongoClient(URI, {});

    try {
        // Connect to MongoDB
        await client.connect();
        console.info('Connected to MongoDB');

        // Select the database
        const database: Db = client.db(DATABASE);
        return database;
    } catch (error: any) {
        console.error('Error connecting to MongoDB:', error);
    }
}


