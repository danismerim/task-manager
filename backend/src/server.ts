import express, { Express, Request, Response } from "express";
import cors from "cors";
import { connectToMongoDB } from "./mongo";
import { getAll, insert } from "./services/task";

const app: Express = express();
app.use(express.json());
app.use(cors());

connectToMongoDB().then((database) => {
    if (!database)
        throw new Error("ERROR - Couldn't connect to the database");

    app.get('/', async (req: Request, res: Response) => {
        const tasks = await getAll(database);
        return res.status(200).json(tasks);
    })

    app.post('/', async (req: Request, res: Response) => {
        const task = req.body// TODO -  content validation - check about nosql injection
        const taskInserted = await insert(database, task.content);
        if (taskInserted)
            return res.status(201).send();
        else
            return res.status(500).send();
    })

    app.listen(3000, () => console.log("Server is running on port 3000"))
})
    .catch(() => {
        console.error("Couldn't connect");

    })