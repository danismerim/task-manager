import { Collection, Db, InsertOneResult, UpdateResult } from 'mongodb';


export const getAll = async (database: Db) => {
    try {
        const collection: Collection = database.collection('tasks');
        const result = await collection.find({}).toArray();;
        return result;
    } catch (err) {
        console.error(`ERROR getting tasks| ${err}`)
        return [];
    }
}

export const insert = async (database: Db, content: string): Promise<boolean> => {
    try {
        const collection: Collection = database.collection('tasks');
        const result: InsertOneResult = await collection.insertOne({ content, done: false });
        return result.acknowledged;
    } catch (err) {
        console.error(`ERROR inserting task ${content} | ${err}`)
        return false;
    }
}

export const updateDone = async (database: Db, id: string, done: boolean): Promise<boolean> => {
    try {
        const collection: Collection = database.collection('tasks');
        const result: UpdateResult = await collection.updateOne({ id }, { done });
        return result.acknowledged;
    } catch (err) {
        console.error(`ERROR updating task id ${id} to done ${done} | ${err}`)
        return false;
    }
}