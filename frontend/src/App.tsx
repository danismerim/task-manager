import { useState } from 'react'
import './App.css'
import { InitialPage } from './components/InitialPage'

export const App = () => {
  return (
    // when needed, Providers can be added here
    <InitialPage />
  )
}


