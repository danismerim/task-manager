import React, { useState } from "react";
import { VIEW_OPTIONS } from "./InitialPage";
import { addTask } from "../services/task";


type CreateOrEditProps = {
    view: string
    setView: React.Dispatch<React.SetStateAction<string>>;
}
export const CreateOrEdit = ({ view, setView }: CreateOrEditProps) => {
    const [task, setTask] = useState<string>();

    // TODO - CreateOrEdit, handle saving redirecting to the list page?
    const saveTask = async (value?: string) => {
        if (!value) return;
        const taskSaved = await addTask(value);
        console.log(taskSaved)
    }

    return (
        <div className="main-content">
            <h3>New Task</h3>
            <input type="text" onChange={(e) => setTask(e.target.value)} />
            <button type="button" onClick={(e) => saveTask(task)} >Add</button>
            <button type="button" onClick={() => setView(VIEW_OPTIONS.MAIN)}>Back</button>
        </div>
    )
}