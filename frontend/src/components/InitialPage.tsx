import { useState } from "react";
import { CreateOrEdit } from "./CreateOrEdit";
import { List } from "./List";

export enum VIEW_OPTIONS {
    MAIN = "MAIN",
    CREATE = "CREATE",
    EDIT = "EDIT",
    LIST = "LIST",
}

export const InitialPage = () => {
    const [view, setView] = useState<string>(VIEW_OPTIONS.MAIN); // move pra useContext?
    return (
        <div className="main-card">
            {view === VIEW_OPTIONS.MAIN ?
                (<div className="main-content">
                    <button type="button" onClick={() => setView(VIEW_OPTIONS.CREATE)}>Add</button>
                    <button type="button" onClick={() => setView(VIEW_OPTIONS.LIST)}>See all</button>
                </div>) :
                view === VIEW_OPTIONS.CREATE || view === VIEW_OPTIONS.EDIT ? (
                    <CreateOrEdit view={view} setView={setView} />
                ) : view === VIEW_OPTIONS.LIST ? (
                    <List setView={setView} />
                )

                    : (<div>TODO</div>)
            }

        </div >
    )
}