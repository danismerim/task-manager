import { useEffect, useState } from "react";
import { Task, getTasks } from "../services/task";
import { VIEW_OPTIONS } from "./InitialPage";

type ListProps = {
    // view: string
    setView: React.Dispatch<React.SetStateAction<string>>;
}

export const List = ({ setView }: ListProps) => {
    const [tasks, setTasks] = useState<Task[]>([]);
    const displayTasks = async () => {
        const getTasksRes = await getTasks();
        if (getTasksRes)
            setTasks(getTasksRes);

        //TODO - display error
    }

    useEffect(() => {
        displayTasks();
    }, [])
    return (<div>
        {tasks.map((task: Task) => (
            <p>{task.content}</p>
        ))}
        <button type="button" onClick={() => setView(VIEW_OPTIONS.MAIN)}>Back</button>
    </div>)
}