const endpointBackend = 'http://localhost:3000/';

// If the project gets bigger, move it to a model folder
export interface Task {
    content: string;
}

export const addTask = async (task: string): Promise<{ error: Boolean, errorDetails?: string }> => {
    try {
        const res = await fetch(`${endpointBackend}`, {
            headers: {
                accept: "application/json",
                "accept-language": "",
                "content-type": "application/json",
            },
            method: "POST",
            body: JSON.stringify({ content: task })
        });

        if (res.status > 299)
            return { error: true, errorDetails: await res.json() };

        return { error: false };
    } catch (error: any) {
        return { error: true, errorDetails: error };
    }
}

export const getTasks = async (): Promise<Task[]> => {
    try {
        const res = await fetch(`${endpointBackend}`, {
            headers: {
                accept: "application/json",
                "accept-language": "",
                "content-type": "application/json",
            },
            method: "GET",
        });
        const tasks: Task[] = await res.json()
        return tasks;
    } catch (error: any) {
        return [];
    }
}
